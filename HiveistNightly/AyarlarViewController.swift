//
//  AyarlarViewController.swift
//  HiveistNightly
//
//  Created by Ulas Can Cengiz on 01/10/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit

class AyarlarViewController: UIViewController {

    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var txtName: UITextField!
    @IBOutlet var txtOccupation: UITextField!
    @IBOutlet var btnSave: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func imageTapped(sender: UIButton)
    {
        // TODO: Select Image
    }

    @IBAction func saveTapped(sender: UIButton)
    {
        // TODO: Check Fields
        // TODO: Save Profile
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
