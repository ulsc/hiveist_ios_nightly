//
//  CalismalarViewController.swift
//  HiveistNightly
//
//  Created by Ulas Can Cengiz on 01/10/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit
import Parse
import CoreLocation

class CalismalarViewController: UIViewController, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    
    var manager : CLLocationManager!
    
    var tableViewDataSource : [PFObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager = CLLocationManager()
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.startUpdatingLocation()

        // Do any additional setup after loading the view.
    }
    
    func getCalismalar()
    {
        let userGeoPoint = PFUser.currentUser()!["location"] as! PFGeoPoint
        
        let query = PFQuery(className:"Calisma")
        
        query.whereKey("location", nearGeoPoint:userGeoPoint)
        query.whereKey("endDate", greaterThan: NSDate())
        query.findObjectsInBackgroundWithBlock {
            (objects: [PFObject]?, error: NSError?) -> Void in
            
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) calisma.")
                // Do something with the found objects
                if let objects = objects as [PFObject]! {
                    self.tableViewDataSource = objects
                    self.tableView.reloadData()
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.userInfo)")
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        manager.delegate = nil
        manager.stopUpdatingLocation()
        
        let user = PFUser.currentUser()!
        
        user["location"] = PFGeoPoint(location: locations[0])
        
        user.saveInBackgroundWithBlock { (success, error) -> Void in
            if success
            {
                self.getCalismalar()
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableViewDataSource.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CalismaCell", forIndexPath: indexPath)
        
        let calisma = tableViewDataSource[indexPath.row]
        
        // Configure the cell...
        cell.textLabel?.text = calisma["title"] as? String
        cell.detailTextLabel?.text = calisma["description"] as? String
        
        return cell
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 70
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        // TODO: Calisma Detay
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
