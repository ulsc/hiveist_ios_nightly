//
//  Constants.swift
//  HiveistNightly
//
//  Created by Ulas Can Cengiz on 02/10/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import Foundation
import UIKit


let PARSE_APP_ID = "PR7O01tVcjtPoZiLuUbSuBnw2TCDKceP3tziKIzS"
let PARSE_CLIENT_KEY = "d8o0v8xnUT49H9goRaOTGQEDIARNXZeZ30687Fiy"

// MARK: Segue Identifiers
let SEGUE_LOGGED_IN = "LoggedIn"
let SEGUE_REGISTER = "Register"

// MARK: ERROR ALERTS
let ERROR_INVALID_CREDENTIALS = UIAlertView(title: "Error", message: "Invalid Credentials", delegate: nil, cancelButtonTitle: "Ok")