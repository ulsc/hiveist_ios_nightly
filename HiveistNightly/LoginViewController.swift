//
//  LoginViewController.swift
//  HiveistNightly
//
//  Created by Ulas Can Cengiz on 01/10/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController {

    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPass: UITextField!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnFacebook: UIButton!
    @IBOutlet var btnRegister: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool)
    {    
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            // Do stuff with the user
            self.performSegueWithIdentifier(SEGUE_LOGGED_IN, sender: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginTapped(sender: UIButton)
    {
        // Login butonuna basıldı
        print("Login Tapped")
        
        PFUser.logInWithUsernameInBackground(txtEmail.text!, password:txtPass.text!) {
            (user: PFUser?, error: NSError?) -> Void in
            if user != nil {
                // Do stuff after successful login.
                self.performSegueWithIdentifier(SEGUE_LOGGED_IN, sender: nil)
            } else {
                // The login failed. Check error to see why.
                
                ERROR_INVALID_CREDENTIALS.show()
            }
        }
        
    }

    @IBAction func facebookTapped(sender: UIButton)
    {
        // Facebook butonuna basıldı
        print("Facebook Tapped")
    }
    
    @IBAction func registerTapped(sender: UIButton)
    {
        // Register butonuna basıldı
        print("Register Tapped")
        
        performSegueWithIdentifier(SEGUE_REGISTER, sender: nil)
    }
    
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == SEGUE_REGISTER
        {
            let vc : RegisterViewController = segue.destinationViewController as! RegisterViewController
            vc.email = txtEmail.text
        }
    }
    

}
