//
//  RegisterViewController.swift
//  HiveistNightly
//
//  Created by Ulas Can Cengiz on 01/10/15.
//  Copyright © 2015 iOSBootcamp. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var txtPass: UITextField!
    @IBOutlet var txtRePass: UITextField!
    @IBOutlet var btnRegister: UIButton!
    
    var email : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        txtEmail.text = email
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func registerTapped(sender: UIButton)
    {
        // TODO: Check the fields
        // TODO: Register user
    }
    
    @IBAction func loginTapped(sender: UIButton)
    {
        dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
